const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
  ];
  
  let isCrossWin = false;
  let isZeroWin = false;
  let isDraw = true;
  
  // Горизонтали
  for (let i = 0; i < 3; i++) {
    if (gameField[i][0] === 'x' && gameField[i][1] === 'x' && gameField[i][2] === 'x') {
      isCrossWin = true;
      isDrow = false;
    }
    if (gameField[i][0] === 'o' && gameField[i][1] === 'o' && gameField[i][2] === 'o') {
      isZeroWin = true;
      isDrow = false;
    }
  }
  
  // Вертикаль
  for (let j = 0; j < 3; j++) {
    if (gameField[0][j] === 'x' && gameField[1][j] === 'x' && gameField[2][j] === 'x') {
      isCrossWin = true;
      isDrow = false;
    }
    if (gameField[0][j] === 'o' && gameField[1][j] === 'o' && gameField[2][j] === 'o') {
      isZeroWin = true;
      isDrow = false;
    }
  }
  
  // Диагональ
  if (gameField[0][0] === 'x' && gameField[1][1] === 'x' && gameField[2][2] === 'x') {
    isCrossWin = true;
    isDrow = false;
  }
  if (gameField[0][0] === 'o' && gameField[1][1] === 'o' && gameField[2][2] === 'o') {
    isZeroWin = true;
    isDrow = false;
  }
  if (gameField[0][2] === 'x' && gameField[1][1] === 'x' && gameField[2][0] === 'x') {
    isCrossWin = true;
    isDrow = false;
  }
  if (gameField[0][2] === 'o' && gameField[1][1] === 'o' && gameField[2][0] === 'o') {
    isZeroWin = true;
    isDrow = false;
  }
  
  if (isCrossWin) {
    console.log('Крестики победили');
  } else if (isZeroWin) {
    console.log('Нолики победили');
  } else if (isDraw) {
    console.log('Ничья');
  } else {
    console.log('Игра еще не окончена');
  }