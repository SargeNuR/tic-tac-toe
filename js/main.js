const cells = document.querySelectorAll('.cell');

let activePlayer = 'X';

const activePlayerEl = document.getElementById('game-step');

cells.forEach(cell => {
    cell.addEventListener('click', () => {
        if (cell.querySelector('img')) {
            return;
        }
        // картинка текущего активного игрока
        const img = document.createElement('img');
        img.src = activePlayer === 'X' ? '../media/xxl-x.svg' : '../media/xxl-zero.svg';
        img.dataset.player = activePlayer;
        cell.appendChild(img);

        // смена
        activePlayer = activePlayer === 'X' ? 'O' : 'X';

        // текст об активном игроке
        let playerName = activePlayer === 'X' ? 'Владелен Пупкин' : 'Екатерина Плюшкина';
        const lastImg = activePlayerEl.querySelector('img');
        if (lastImg) {
            const lastPlayerName = lastImg.dataset.player === 'X' ? 'Владелен Пупкин' : 'Екатерина Плюшкина';
            if (cell.querySelector(`img[data-player="${lastImg.dataset.player}"]`)) {
                playerName = lastPlayerName;
            }
        }
        activePlayerEl.innerHTML = `Ходит&nbsp;<img src="../media/${activePlayer.toLowerCase()}.svg"/>&nbsp;${playerName}`;
    });
});