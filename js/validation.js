const loginInput = document.querySelector('.login');
const passwordInput = document.querySelector('.password');

// Запрет копирования введенных значений
loginInput.addEventListener('copy', (event) => {
    event.preventDefault();
});
passwordInput.addEventListener('copy', (event) => {
    event.preventDefault();
});

// Валидация полей Логин и Пароль
const loginRegex = /^[a-zA-Z0-9._]+$/;
const passwordRegex = /^[a-zA-Z0-9._]+$/;

function validateLoginInput() {
    if (!loginRegex.test(loginInput.value)) {
        loginInput.setCustomValidity('Логин должен содержать только английские буквы, цифры и знаки ".", "_"');
        return false;
    } else {
        loginInput.setCustomValidity('');
        return true;
    }
}

function validatePasswordInput() {
    if (!passwordRegex.test(passwordInput.value)) {
        passwordInput.setCustomValidity('Пароль должен содержать только английские буквы, цифры и знаки ".", "_"');
        return false;
    } else {
        passwordInput.setCustomValidity('');
        return true;
    }
}

loginInput.addEventListener('input', () => {
    validateLoginInput();
});

passwordInput.addEventListener('input', () => {
    validatePasswordInput();
});

// Обработка события нажатия на кнопку "Войти"
const form = document.querySelector('form');
form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (validateLoginInput() && validatePasswordInput()) {
        console.log(`Логин: ${loginInput.value}, пароль: ${passwordInput.value}`);
    }
});